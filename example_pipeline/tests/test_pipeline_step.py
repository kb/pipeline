# -*- encoding: utf-8 -*-
import pytest

from example_pipeline.service import Download
from pipeline.models import Pipeline, PipelineStep


@pytest.mark.django_db
def test_step_class():
    pipeline = Pipeline.objects.init_pipeline("apple", "Apple", False)
    step = PipelineStep.objects.init_pipeline_step(
        pipeline,
        1,
        "Document download",
        "example_pipeline",
        "service",
        "Download",
    )
    assert Download == step.pipeline_step_class()
