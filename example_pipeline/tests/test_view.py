# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from login.tests.factories import TEST_PASSWORD, UserFactory
from pipeline.models import (
    Pipeline,
    PipelineProcess,
    PipelineProcessStep,
    PipelineStep,
)


def _pipeline_process_step():
    pipeline = Pipeline.objects.init_pipeline("apple", "Apple", False)
    PipelineStep.objects.init_pipeline_step(
        pipeline,
        1,
        "Document download",
        "example_pipeline",
        "service",
        "Download",
    )
    PipelineStep.objects.init_pipeline_step(
        pipeline, 2, "Document upload", "example_pipeline", "service", "Upload"
    )
    assert 0 == PipelineProcessStep.objects.count()
    PipelineProcess.objects.create_pipeline_process(pipeline)
    assert 1 == PipelineProcessStep.objects.count()
    return PipelineProcessStep.objects.first()


@pytest.mark.django_db
def test_process_detail(client):
    user = UserFactory(is_staff=True)
    process_step = _pipeline_process_step()
    process_step.create_audit("Apple")
    process_step.create_audit("Orange")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse(
            "pipeline.process.detail", args=[process_step.pipeline_process.pk]
        )
    )
    # check
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_process_list(client):
    user = UserFactory(is_staff=True)
    _pipeline_process_step()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("pipeline.process.list"))
    # check
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_process_outstanding_ignore_retries(client):
    user = UserFactory(is_staff=True)
    _pipeline_process_step()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("pipeline.process.outstanding.list"))
    # check
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_process_step_list(client):
    user = UserFactory(is_staff=True)
    _pipeline_process_step()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("pipeline.process.step.list"))
    # check
    assert HTTPStatus.OK == response.status_code
