# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command


@pytest.mark.django_db
def test_demo_data():
    call_command("demo_data_pipeline")


@pytest.mark.django_db
def test_create_test_pipeline_process():
    call_command("demo_data_pipeline")
    call_command("create_test_pipeline_process")


@pytest.mark.django_db
def test_pipeline_process_outstanding_steps():
    call_command("demo_data_pipeline")
    call_command("create_test_pipeline_process")
    call_command("pipeline_process_outstanding_steps")
