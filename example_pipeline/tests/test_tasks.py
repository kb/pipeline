# -*- encoding: utf-8 -*-
import pytest
import pytz

from datetime import datetime
from django.core.management import call_command
from freezegun import freeze_time

from pipeline.models import Pipeline, PipelineProcess, PipelineStep
from pipeline.tasks import (
    process_next_step,
    process_outstanding_steps,
    process_step,
)


@pytest.mark.django_db
def test_process_next_step():
    call_command("demo_data_pipeline")
    call_command("create_test_pipeline_process")
    pipeline = Pipeline.objects.init_pipeline("apple", "Apple", False)
    PipelineStep.objects.init_pipeline_step(
        pipeline,
        1,
        "Example Download",
        "example_pipeline",
        "service",
        "Download",
    )
    pipeline_process = PipelineProcess.objects.create_pipeline_process(pipeline)
    process_next_step(pipeline_process.pk)


@pytest.mark.django_db
def test_process_outstanding_steps():
    call_command("demo_data_pipeline")
    call_command("create_test_pipeline_process")
    process_outstanding_steps()


@pytest.mark.django_db
def test_process_step():
    call_command("demo_data_pipeline")
    call_command("create_test_pipeline_process")
    pipeline = Pipeline.objects.init_pipeline("apple", "Apple", False)
    PipelineStep.objects.init_pipeline_step(
        pipeline,
        1,
        "Example Download",
        "example_pipeline",
        "service",
        "Download",
    )
    pipeline_process_step = PipelineProcess.objects.create_pipeline_process(
        pipeline
    )
    process_step(pipeline_process_step.pk)
    pipeline_process_step.refresh_from_db()
    assert pipeline_process_step.is_complete() is True


@pytest.mark.django_db
def test_process_step_already_complete():
    """Step is already complete, so don't throw an error.

    If the pipeline step has already been complete (perhaps via a retry)...
    https://www.kbsoftware.co.uk/crm/ticket/5423/

    """
    call_command("demo_data_pipeline")
    call_command("create_test_pipeline_process")
    pipeline = Pipeline.objects.init_pipeline("apple", "Apple", False)
    PipelineStep.objects.init_pipeline_step(
        pipeline,
        1,
        "Example Download",
        "example_pipeline",
        "service",
        "Download",
    )
    pipeline_process_step = PipelineProcess.objects.create_pipeline_process(
        pipeline
    )
    with freeze_time(datetime(2020, 3, 11, 18, 30, 0, tzinfo=pytz.utc)):
        pipeline_process_step.set_complete()
    process_step(pipeline_process_step.pk)
    pipeline_process_step.refresh_from_db()
    # check the complete date / time didn't get updated
    assert (
        datetime(2020, 3, 11, 18, 30, 0, tzinfo=pytz.utc)
        == pipeline_process_step.completed_date
    )
