# -*- encoding: utf-8 -*-
import pytest
import pytz

from datetime import date, datetime
from freezegun import freeze_time

from pipeline.models import (
    Pipeline,
    PipelineError,
    PipelineProcess,
    PipelineProcessStep,
    PipelineStatus,
    PipelineStep,
)


def _process_step(class_name):
    pipeline = Pipeline.objects.init_pipeline("apple", "Apple", False)
    PipelineStep.objects.init_pipeline_step(
        pipeline, 1, class_name, "example_pipeline", "service", class_name
    )
    PipelineStep.objects.init_pipeline_step(
        pipeline, 2, "Upload document", "example_pipeline", "service", "Upload"
    )
    assert 0 == PipelineProcessStep.objects.count()
    PipelineProcess.objects.create_pipeline_process(pipeline)
    assert 1 == PipelineProcessStep.objects.count()
    return PipelineProcessStep.objects.first()


@pytest.mark.django_db
def test_audit():
    process_step = _process_step("Download")
    process_step.create_audit("Oranges and Lemons")
    process_step.create_audit("Apples and Peaches")
    assert ["Apples and Peaches", "Oranges and Lemons"] == [
        x.description for x in process_step.audit()
    ]


@pytest.mark.django_db
def test_create_next_step():
    _process_step("Download")
    assert 1 == PipelineProcessStep.objects.count()
    pipeline_process_step = PipelineProcessStep.objects.first()
    next_step = pipeline_process_step.create_next_step()
    assert next_step is not None
    assert "" == next_step.queue_name


@pytest.mark.django_db
def test_is_pipeline_complete():
    _process_step("Download")
    assert 1 == PipelineProcessStep.objects.count()
    PipelineProcessStep.objects.process()
    assert 2 == PipelineProcessStep.objects.count()
    process_step_1 = PipelineProcessStep.objects.get(pipeline_step__order=1)
    process_step_1.set_complete()
    process_step_2 = PipelineProcessStep.objects.get(pipeline_step__order=2)
    process_step_2.set_complete()
    assert process_step_1.is_complete() is True
    assert process_step_2.is_complete() is True
    assert process_step_2.is_pipeline_complete() is True


@pytest.mark.django_db
def test_is_pipeline_complete_not_v1():
    _process_step("Download")
    assert 1 == PipelineProcessStep.objects.count()
    process_step_1 = PipelineProcessStep.objects.first()
    process_step_1.set_complete()
    assert process_step_1.is_complete() is True
    assert process_step_1.is_pipeline_complete() is False


@pytest.mark.django_db
def test_is_pipeline_complete_not_v2():
    _process_step("Download")
    assert 1 == PipelineProcessStep.objects.count()
    process_step_1 = PipelineProcessStep.objects.first()
    assert process_step_1.is_complete() is False
    assert process_step_1.is_pipeline_complete() is False


@pytest.mark.django_db
def test_objects_process():
    _process_step("Download")
    assert 1 == PipelineProcessStep.objects.count()
    PipelineProcessStep.objects.process()
    assert 2 == PipelineProcessStep.objects.count()
    process_step_1 = PipelineProcessStep.objects.get(pipeline_step__order=1)
    process_step_2 = PipelineProcessStep.objects.get(pipeline_step__order=2)
    assert (
        process_step_1.completed_date is not None
    ), "'process_step_1' is not complete"
    assert process_step_1.completed_date.date() == date.today()
    assert 0 == process_step_1.retries
    assert process_step_2.completed_date is None
    assert 0 == process_step_2.retries


@pytest.mark.django_db
def test_pipeline_status():
    """What is the status of this step in the pipeline process?"""
    with freeze_time(datetime(2014, 10, 1, 6, 0, 0, tzinfo=pytz.utc)):
        process_step = _process_step("Download")
    assert PipelineStatus(
        onedrive_folder_name="wip",
        status="Download (pending since 01/10/2014 07:00)",
        pending=True,
    ) == process_step.pipeline_status("wip")


@pytest.mark.django_db
def test_process():
    process_step = _process_step("Download")
    assert 1 == PipelineProcessStep.objects.count()
    assert process_step.process() is True
    assert 2 == PipelineProcessStep.objects.count()
    process_step_1 = PipelineProcessStep.objects.get(pipeline_step__order=1)
    process_step_2 = PipelineProcessStep.objects.get(pipeline_step__order=2)
    # the step is completed by 'objects.process' (not by 'ProcessStep.process')
    assert process_step_1.completed_date is None
    assert process_step_2.completed_date is None
    assert 0 == process_step_1.retries
    assert 0 == process_step_2.retries


@pytest.mark.django_db
def test_process_no_run_method():
    process_step = _process_step("DownloadNoProcessMethod")
    with pytest.raises(PipelineError) as e:
        process_step.process()
    assert (
        "PipelineError, AttributeError: 'DownloadNoProcessMethod' "
        "object has no attribute 'process' "
        "(does <class 'example_pipeline.service.DownloadNoProcessMethod'> "
        "contain a 'process' method)?"
    ) in str(e.value)
