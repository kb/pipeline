# -*- encoding: utf-8 -*-
from django.core.management import call_command
from django.core.management.base import BaseCommand

from pipeline.models import PipelineProcess, Pipeline


class Command(BaseCommand):
    help = "Pipeline - create test process"

    def handle(self, *args, **options):
        self.stdout.write("{}...".format(self.help))
        pipeline = Pipeline.objects.get(slug="test")
        pipeline_process = PipelineProcess.objects.create_pipeline_process(
            pipeline
        )
        self.stdout.write(
            "Process {} created for pipeline: {}".format(
                pipeline_process.pk, pipeline
            )
        )
        # will run process_steps.send()
        call_command("pipeline_process_outstanding_steps")
        self.stdout.write("Process steps (background task)...")
        self.stdout.write("{} - Complete".format(self.help))
