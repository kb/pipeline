# -*- encoding: utf-8 -*-
from django.core.management import call_command
from django.core.management.base import BaseCommand

from pipeline.models import Pipeline, PipelineStep


class Command(BaseCommand):
    help = "Pipeline demo data"

    def handle(self, *args, **options):
        self.stdout.write("{}...".format(self.help))
        pipeline = Pipeline.objects.init_pipeline("test", "Testing", False)
        print(
            PipelineStep.objects.init_pipeline_step(
                pipeline,
                1,
                "Process one",
                "example_pipeline",
                "service",
                "ProcessOne",
            )
        )
        print(
            PipelineStep.objects.init_pipeline_step(
                pipeline,
                2,
                "Process two",
                "example_pipeline",
                "service",
                "ProcessTwo",
            )
        )
        print(
            PipelineStep.objects.init_pipeline_step(
                pipeline,
                3,
                "Process three",
                "example_pipeline",
                "service",
                "ProcessThree",
            )
        )
        call_command("create_test_pipeline_process")
        self.stdout.write("{} - Complete".format(self.help))
