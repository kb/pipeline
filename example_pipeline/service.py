# -*- encoding: utf-8 -*-
import logging
import random
import time

from pipeline.models import PipelineError

logger = logging.getLogger(__name__)


class PipelineProcessMixin:
    def __init__(self, process_step):
        self.process_step = process_step

    def _log(self, message=None):
        x = "{}. {}{}".format(
            self.process_step.pk,
            self.__class__.__name__,
            ": {}".format(message) if message else "",
        )
        logger.info(x)
        self.process_step.create_audit(x)


class Download(PipelineProcessMixin):
    def process(self):
        return True


class DownloadNoProcessMethod(PipelineProcessMixin):
    pass


class ProcessOne(PipelineProcessMixin):
    def process(self):
        self._log("Success")
        time.sleep(1)
        return True


class ProcessTwo(PipelineProcessMixin):
    def process(self):
        raise_error = random.choice([False, True, False])
        if raise_error:
            message = "Random fail"
            self._log(message)
            raise PipelineError(
                "{} for '{}'".format(message, self.__class__.__name__)
            )
        else:
            self._log("Success")
        time.sleep(1)
        return True


class ProcessThree(PipelineProcessMixin):
    def process(self):
        self._log("Success")
        time.sleep(1)
        return True
