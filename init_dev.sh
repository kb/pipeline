#!/bin/bash
# exit immediately if a command exits with a nonzero exit status.
set -e
# treat unset variables as an error when substituting.
set -u

echo Drop database: $DATABASE_NAME
psql -U $DATABASE_USER -c "DROP DATABASE IF EXISTS $DATABASE_NAME;"
psql -U $DATABASE_USER -c "CREATE DATABASE $DATABASE_NAME TEMPLATE=template0 ENCODING='utf-8';"

django-admin migrate --noinput
django-admin demo_data_login
django-admin init_app_pipeline
django-admin demo_data_pipeline
django-admin runserver 0.0.0.0:8000
