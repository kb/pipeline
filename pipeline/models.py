# -*- encoding: utf-8 -*-
import attr
import importlib
import logging
import pathlib

from django.db import models, transaction
from django.utils import timezone
from urllib.parse import parse_qs, urlparse, urlunparse
from urllib.parse import urljoin

from base.model_utils import (
    RetryModel,
    RetryModelManager,
    TimeStampedModel,
    TimedCreateModifyDeleteModel,
)
from pipeline.tasks import process_next_step


logger = logging.getLogger(__name__)


@attr.s
class PipelineStatus:
    status = attr.ib()
    # used by 'document_url_to_desktop' and 'document_url_to_desktop_caption'
    onedrive_folder_name = attr.ib()
    button_caption = attr.ib(default=None)
    document_url = attr.ib(default=None)
    folder_url = attr.ib(default=None)
    pending = attr.ib(default=False)
    comment_required = attr.ib(default=False)

    def _file_name_etc(self):
        file_name = netloc = path = scheme = None
        if self.document_url:
            x = urlparse(self.document_url)
            pos = x.path.find("_layouts")
            if pos == -1:
                logger.info(
                    "Cannot find '_layouts' in OneDrive URL: {}".format(
                        self.document_url
                    )
                )
            else:
                params = parse_qs(x.query)
                file_param = params.get("file")
                if file_param and len(file_param) == 1:
                    file_name = file_param[0]
                    path = urljoin(
                        x.path[:pos],
                        "Documents/{}/{}".format(
                            self.onedrive_folder_name, file_name
                        ),
                    )
                    netloc = x.netloc
                    scheme = x.scheme
                else:
                    raise PipelineError(
                        "Cannot find 'file' in OneDrive URL parameters: {}".format(
                            self.document_url
                        )
                    )
        return file_name, netloc, path, scheme

    def document_url_to_desktop(self):
        result = None
        file_name, netloc, path, scheme = self._file_name_etc()
        if file_name:
            extension = pathlib.Path(file_name).suffix
            sharepoint = None
            if extension == ".docx":
                sharepoint = "ms-word:ofe|u|"
            elif extension == ".xlsx":
                sharepoint = "ms-excel:ofe|u|"
            if sharepoint:
                fragment = params = query = ""
                result = "{}{}".format(
                    sharepoint,
                    urlunparse((scheme, netloc, path, params, query, fragment)),
                )
        return result

    def document_url_to_desktop_caption(self):
        result = None
        file_name, _, _, _ = self._file_name_etc()
        if file_name:
            extension = pathlib.Path(file_name).suffix
            if extension == ".docx":
                result = "Open in Desktop Word..."
            elif extension == ".xlsx":
                result = "Open in Desktop Excel..."
        return result


class PipelineError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("%s, %s" % (self.__class__.__name__, self.value))


class PipelineManager(models.Manager):
    def _create_pipeline(
        self, slug, title, comment_required, next_pipeline=None
    ):
        x = self.model(
            slug=slug,
            title=title,
            comment_required=comment_required,
            next_pipeline=next_pipeline or "",
        )
        x.save()
        return x

    def init_pipeline(self, slug, title, comment_required, next_pipeline=None):
        try:
            x = self.model.objects.get(slug=slug)
            x.comment_required = comment_required
            x.next_pipeline = next_pipeline or ""
            x.title = title
            x.save()
        except self.model.DoesNotExist:
            x = self._create_pipeline(
                slug, title, comment_required, next_pipeline
            )
        return x


class Pipeline(models.Model):
    slug = models.SlugField(unique=True)
    title = models.CharField(max_length=200)
    comment_required = models.BooleanField(
        help_text="Should the user enter a comment to start this pipeline?"
    )
    next_pipeline = models.SlugField(
        "The next pipeline (when all steps for this pipeline are complete)"
    )
    objects = PipelineManager()

    class Meta:
        ordering = ("slug",)
        verbose_name = "Pipeline"
        verbose_name_plural = "Pipelines"

    def __str__(self):
        return "{} ('{}')".format(self.title, self.slug)

    def first_step(self):
        return self.steps().order_by("order").first()

    def next_step(self, current_step):
        result = None
        found_step = False
        for x in self.steps().order_by("order"):
            if found_step:
                result = x
                break
            if x == current_step:
                found_step = True
        if not found_step:
            raise PipelineError(
                "Cannot find current step ({}) "
                "(trying to find the 'next_step' for pipeline '{}')".format(
                    current_step, self
                )
            )
        return result

    def steps(self):
        return self.pipelinestep_set.exclude(deleted=True)


class PipelineStepManager(models.Manager):
    def _create_pipeline_step(
        self, pipeline, order, description, app, module, class_name
    ):
        x = self.model(
            pipeline=pipeline,
            order=order,
            description=description,
            app=app,
            module=module,
            class_name=class_name,
        )
        x.save()
        return x

    def init_pipeline_step(
        self, pipeline, order, description, app, module, class_name
    ):
        try:
            x = self.model.objects.get(pipeline=pipeline, order=order)
            x.app = app
            x.description = description
            x.module = module
            x.class_name = class_name
            if x.is_deleted:
                x.undelete()
            else:
                x.save()
        except self.model.DoesNotExist:
            x = self._create_pipeline_step(
                pipeline, order, description, app, module, class_name
            )
        return x


class PipelineStep(TimedCreateModifyDeleteModel):
    pipeline = models.ForeignKey(Pipeline, on_delete=models.CASCADE)
    order = models.IntegerField()
    description = models.TextField(
        help_text="Description of the step displayed to the user"
    )
    # app, module and *class_name* were copied from the 'report' app...
    app = models.CharField(max_length=100)
    module = models.CharField(max_length=100)
    class_name = models.CharField(max_length=100)
    objects = PipelineStepManager()

    class Meta:
        ordering = ("pipeline__slug", "order")
        unique_together = ("pipeline", "order")
        verbose_name = "Pipeline step"
        verbose_name_plural = "Pipeline steps"

    def __str__(self):
        return "'{}' process - step {}: '{}'".format(
            self.pipeline.slug, self.order, self.class_name
        )

    def pipeline_step_class(self):
        """Import the report module."""
        class_name = "{}.{}".format(self.app, self.module)
        try:
            step_module = importlib.import_module(class_name)
        except ImportError as e:
            # ``ModuleNotFoundError`` not available until python 3.6
            message = (
                "Cannot find class '{}'. Do you have '{}.py' "
                "in your '{}' package? ({})".format(
                    self.class_name, self.module, self.app, str(e)
                )
            )
            raise PipelineError(message)
        return getattr(step_module, self.class_name)


class PipelineProcessManager(models.Manager):
    def create_pipeline_process(self, pipeline):
        """Create a pipeline process.

        .. note:: This method returns the ``PipelineProcessStep``
                  (not the ``PipelineProcess``).
                  This is so we can send a background task for the step.

        """
        pipeline_process = self.model(pipeline=pipeline)
        pipeline_process.save()
        pipeline_step = pipeline.first_step()
        if pipeline_step:
            pipeline_process_step = PipelineProcessStep(
                pipeline_process=pipeline_process,
                pipeline_step=pipeline_step,
                max_retry_count=PipelineProcessStep.DEFAULT_MAX_RETRY_COUNT,
            )
            pipeline_process_step.save()
        else:
            raise PipelineError(
                "Cannot create 'PipelineProcess', 'Pipeline' '{}' "
                "has no steps".format(pipeline.slug)
            )
        return pipeline_process_step

    def current(self):
        return self.model.objects.exclude(deleted=True)

    def outstanding_ignore_retries(self):
        pks = set(
            PipelineProcessStep.objects.outstanding_ignore_retries().values_list(
                "pipeline_process__pk", flat=True
            )
        )
        return self.model.objects.current().filter(pk__in=pks)

    def pipeline_status(self, pipeline_process_pks, onedrive_folder_name):
        pipeline_process_step = self.step_for_pks(pipeline_process_pks)
        return pipeline_process_step.pipeline_status(onedrive_folder_name)

    def step_for_pks(self, pipeline_process_pks):
        """What is the most recent step for these process IDs?

        PJK 15/07/2020, We were using the ``created`` date (to find the most
        recent step) but this doesn't always seem to work:
        https://www.kbsoftware.co.uk/crm/ticket/5081/

        """
        pipeline_process_step = (
            PipelineProcessStep.objects.filter(
                pipeline_process__pk__in=pipeline_process_pks
            )
            .order_by("-pk")
            .first()
        )
        if not pipeline_process_step:
            raise PipelineError(
                "Cannot find a step for a pipeline process in {}".format(
                    pipeline_process_pks
                )
            )
        return pipeline_process_step


class PipelineProcess(TimedCreateModifyDeleteModel):
    pipeline = models.ForeignKey(Pipeline, on_delete=models.CASCADE)
    objects = PipelineProcessManager()

    class Meta:
        ordering = ("-pk",)
        verbose_name = "Pipeline process"
        verbose_name_plural = "Pipeline processes"

    def __str__(self):
        return "Process {} for pipeline '{}' (created {})".format(
            self.pk, self.pipeline.slug, self.created.strftime("%d/%m/%Y %H:%M")
        )

    def steps(self):
        return self.pipelineprocessstep_set.all()


class PipelineProcessStepManager(RetryModelManager):
    def current(self):
        """Return the current steps.

        This is used by the ``outstanding`` method in ``RetryModelManager``.

        - Exclude steps where the ``pipeline_process`` has been deleted.
        - Completed items are excluded in the ``outstanding`` method.

        """
        return self.model.objects.exclude(pipeline_process__deleted=True)


class PipelineProcessStep(RetryModel):
    """One step in a process.

    PJK 15/07/2020, We were using the ``created`` date in the ``step_for_pks``
    method (to find the most recent step), but this doesn't always seem to
    work:
    https://www.kbsoftware.co.uk/crm/ticket/5081/

    """

    DEFAULT_MAX_RETRY_COUNT = 5

    pipeline_process = models.ForeignKey(
        PipelineProcess, on_delete=models.CASCADE
    )
    pipeline_step = models.ForeignKey(PipelineStep, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    objects = PipelineProcessStepManager()

    class Meta:
        ordering = ("-pk",)
        unique_together = ("pipeline_process", "pipeline_step")
        verbose_name = "Pipeline process step"
        verbose_name_plural = "Pipeline process steps"

    def __str__(self):
        return "Pipeline process step {} for process {} (created {})".format(
            self.pk,
            self.pipeline_process.pk,
            self.created.strftime("%d/%m/%Y %H:%M"),
        )

    def _pipeline_step_class_instance(self):
        result = None
        step_class = self.pipeline_step.pipeline_step_class()
        # create an instance of the step class
        result = step_class(self)
        try:
            # check the step class has a 'process' method
            result.process
        except AttributeError as e:
            raise PipelineError(
                "AttributeError: {} (does {} contain a 'process' "
                "method)?".format(str(e), step_class)
            )
        return result

    def audit(self):
        return self.pipelineprocessstepaudit_set.all()

    def create_audit(self, description):
        return (
            PipelineProcessStepAudit.objects.create_pipeline_process_step_audit(
                self, description
            )
        )

    def create_next_step(self):
        result = None
        next_step = self.pipeline_process.pipeline.next_step(self.pipeline_step)
        if next_step:
            result = PipelineProcessStep(
                pipeline_process=self.pipeline_process,
                pipeline_step=next_step,
                max_retry_count=self.DEFAULT_MAX_RETRY_COUNT,
                queue_name="",
            )
            result.save()
        return result

    def is_pipeline_complete(self):
        """Is the pipeline complete?

        Make sure this step is complete, then check the pipeline to see if there
        is another step after this one.

        """
        result = False
        if self.is_complete():
            next_step = self.pipeline_process.pipeline.next_step(
                self.pipeline_step
            )
            if next_step:
                # more steps, so the pipeline is not complete
                pass
            else:
                # no more steps, so the pipeline is complete
                result = True
        return result

    def pipeline_status(self, onedrive_folder_name):
        """What is the status of this step in the pipeline process?"""
        description = self.pipeline_step.description
        if self.is_complete():
            result = PipelineStatus(
                onedrive_folder_name=onedrive_folder_name,
                status="{} (completed {})".format(
                    description,
                    timezone.localtime(self.completed_date).strftime(
                        "%d/%m/%Y %H:%M"
                    ),
                ),
            )
        elif self.retries:
            result = PipelineStatus(
                onedrive_folder_name=onedrive_folder_name,
                status="{} (tried {} time{})".format(
                    description, self.retries, "" if self.retries == 1 else "s"
                ),
            )
        else:
            result = PipelineStatus(
                pending=True,
                onedrive_folder_name=onedrive_folder_name,
                status="{} (pending since {})".format(
                    description,
                    timezone.localtime(self.created).strftime("%d/%m/%Y %H:%M"),
                ),
            )
        return result

    def process(self):
        """Create an instance of the step class and call the process method."""
        result = False
        class_instance = self._pipeline_step_class_instance()
        if class_instance.process():
            next_process_step = self.create_next_step()
            if next_process_step:
                transaction.on_commit(
                    lambda: process_next_step.send_with_options(
                        args=(next_process_step.pk,)
                    )
                )
            result = True
        return result


class PipelineProcessStepAuditManager(models.Manager):
    def create_pipeline_process_step_audit(
        self, pipeline_process_step, description
    ):
        x = PipelineProcessStepAudit(
            pipeline_process_step=pipeline_process_step, description=description
        )
        x.save()
        return x


class PipelineProcessStepAudit(TimeStampedModel):
    pipeline_process_step = models.ForeignKey(
        PipelineProcessStep, on_delete=models.CASCADE
    )
    description = models.TextField()
    objects = PipelineProcessStepAuditManager()

    class Meta:
        ordering = ("-pk",)
        verbose_name = "Process step audit"
        verbose_name_plural = "Process step audit"

    def __str__(self):
        return "{} ({}): {}".format(
            self.pipeline_process_step.pk,
            self.created.strftime("%d/%m/%Y %H:%M"),
            self.description,
        )
