# -*- encoding: utf-8 -*-
import pytest

from login.tests.factories import UserFactory
from pipeline.models import Pipeline
from .factories import PipelineFactory, PipelineStepFactory


@pytest.mark.django_db
def test_first_step():
    pipeline = Pipeline.objects.init_pipeline("apple", "Apple", False)
    step = PipelineStepFactory(pipeline=pipeline, order=1)
    step.set_deleted(UserFactory())
    first_step = PipelineStepFactory(pipeline=pipeline, order=2)
    PipelineStepFactory(pipeline=PipelineFactory(), order=1)
    assert first_step == pipeline.first_step()


@pytest.mark.django_db
@pytest.mark.parametrize(
    "comment_required",
    [
        True,
        False,
    ],
)
def test_init(comment_required):
    x = Pipeline.objects.init_pipeline("apple", "Apple", comment_required)
    assert "apple" == x.slug
    assert "Apple" == x.title
    assert "" == x.next_pipeline
    assert x.comment_required is comment_required


@pytest.mark.django_db
@pytest.mark.parametrize(
    "comment_required",
    [
        True,
        False,
    ],
)
def test_init_next_pipeline(comment_required):
    x = Pipeline.objects.init_pipeline(
        "apple", "Apple", comment_required, "orange"
    )
    assert "apple" == x.slug
    assert "Apple" == x.title
    assert "orange" == x.next_pipeline
    assert x.comment_required is comment_required


@pytest.mark.django_db
@pytest.mark.parametrize(
    "comment_required",
    [
        True,
        False,
    ],
)
def test_init_already_exists(comment_required):
    opposite = not comment_required
    PipelineFactory(slug="apple", title="Orange", comment_required=opposite)
    assert 1 == Pipeline.objects.count()
    x = Pipeline.objects.init_pipeline("apple", "Apple", comment_required)
    assert 1 == Pipeline.objects.count()
    assert "apple" == x.slug
    assert "Apple" == x.title
    assert x.comment_required is comment_required


@pytest.mark.django_db
def test_next_step():
    pipeline = Pipeline.objects.init_pipeline("apple", "Apple", False)
    step_1 = PipelineStepFactory(pipeline=pipeline, order=1)
    step_2 = PipelineStepFactory(pipeline=pipeline, order=2)
    step_2.set_deleted(UserFactory())
    step_3 = PipelineStepFactory(pipeline=pipeline, order=3)
    PipelineStepFactory(pipeline=pipeline, order=4)
    PipelineStepFactory(pipeline=PipelineFactory(), order=3)
    assert step_3 == pipeline.next_step(step_1)


@pytest.mark.django_db
def test_order():
    PipelineFactory(slug="orange")
    PipelineFactory(slug="apple")
    assert ["apple", "orange"] == [x.slug for x in Pipeline.objects.all()]


@pytest.mark.django_db
def test_steps():
    pipeline = Pipeline.objects.init_pipeline("apple", "Apple", False)
    step_1 = PipelineStepFactory(pipeline=pipeline, order=1)
    step_2 = PipelineStepFactory(pipeline=pipeline, order=2)
    step_3 = PipelineStepFactory(pipeline=pipeline, order=3)
    step_3.set_deleted(UserFactory())
    step_4 = PipelineStepFactory(pipeline=pipeline, order=4)
    PipelineStepFactory(pipeline=PipelineFactory(), order=4)
    assert [step_1.pk, step_2.pk, step_4.pk] == [x.pk for x in pipeline.steps()]


@pytest.mark.django_db
def test_str():
    x = PipelineFactory(slug="abc", title="Apple")
    assert "Apple ('abc')" == str(x)
