# -*- encoding: utf-8 -*-
import pytest

from pipeline.models import PipelineStatus


def test_pipeline_status():
    x = PipelineStatus(status="apple", onedrive_folder_name="ABC")


def test_document_url_to_desktop_docx():
    x = PipelineStatus(
        status="apple",
        onedrive_folder_name="ABC",
        document_url=(
            "https://hatherleigh-my.sharepoint.com/"
            "personal/p_kimber_hatherleigh_info/"
            "_layouts/15/Doc.aspx?"
            "sourcedoc=%7BCA6DED98-2F71-48B0-B63D-EEC368DF6630%7D"
            "&file=TEST-123.docx"
            "&action=default&mobileredirect=true"
        ),
    )
    assert (
        "ms-word:ofe|u|"
        "https://hatherleigh-my.sharepoint.com/"
        "personal/p_kimber_hatherleigh_info/"
        "Documents/ABC/TEST-123.docx"
    ) == x.document_url_to_desktop()


@pytest.mark.parametrize(
    "document_url",
    [
        (
            "https://hatherleigh-my.sharepoint.com/"
            "personal/p_kimber_hatherleigh_info/"
            "_layouts/15/Doc.aspx?"
            "sourcedoc=%7BCA6DED98-2F71-48B0-B63D-EEC368DF6630%7D"
            "&file=TEST-123.pptx"
            "&action=default&mobileredirect=true"
        ),
        (
            "https://navigatorterminals-my.sharepoint.com/"
            "personal/catherine_johnson_navigatorterminals_com/"
            "Documents/NMS/NTS-MSDS-10043-2020-11-30-1103-25734.pdf"
        ),
    ],
)
def test_document_url_to_desktop_not_handled(document_url):
    """We only handle Excel and Word files so far...

    The PDF example is from:
    https://www.kbsoftware.co.uk/crm/ticket/5381/

    """
    x = PipelineStatus(
        status="apple",
        onedrive_folder_name="ABC",
        document_url=document_url,
    )
    assert x.document_url_to_desktop() is None


def test_document_url_to_desktop_xlsx():
    x = PipelineStatus(
        status="apple",
        onedrive_folder_name="ABC",
        document_url=(
            "https://hatherleigh-my.sharepoint.com/"
            "personal/p_kimber_hatherleigh_info/"
            "_layouts/15/Doc.aspx?"
            "sourcedoc=%7BCA6DED98-2F71-48B0-B63D-EEC368DF6630%7D"
            "&file=TEST-123.xlsx"
            "&action=default&mobileredirect=true"
        ),
    )
    assert (
        "ms-excel:ofe|u|"
        "https://hatherleigh-my.sharepoint.com/"
        "personal/p_kimber_hatherleigh_info/"
        "Documents/ABC/TEST-123.xlsx"
    ) == x.document_url_to_desktop()


@pytest.mark.parametrize(
    "file_name,expect",
    [
        ("TEST-123.xlsx", "Open in Desktop Excel..."),
        ("TEST-123.docx", "Open in Desktop Word..."),
        ("TEST-123.png", None),
    ],
)
def test_document_url_to_desktop_caption(file_name, expect):
    x = PipelineStatus(
        status="apple",
        onedrive_folder_name="ABC",
        document_url=(
            "https://hatherleigh-my.sharepoint.com/"
            "personal/p_kimber_hatherleigh_info/"
            "_layouts/15/Doc.aspx?"
            "sourcedoc=%7BCA6DED98-2F71-48B0-B63D-EEC368DF6630%7D"
            "&file={}"
            "&action=default&mobileredirect=true".format(file_name)
        ),
    )
    assert expect == x.document_url_to_desktop_caption()


def test_document_url_to_desktop_empty():
    x = PipelineStatus(
        status="apple",
        onedrive_folder_name="ABC",
        document_url=None,
    )
    assert x.document_url_to_desktop() is None
