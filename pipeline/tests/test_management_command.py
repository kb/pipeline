# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command


@pytest.mark.django_db
def test_pipeline_process():
    call_command("pipeline_process")


@pytest.mark.django_db
def test_pipeline_process_outstanding_steps():
    call_command("pipeline_process_outstanding_steps")


@pytest.mark.django_db
def test_pipeline_test_task():
    call_command("pipeline_test_task")


@pytest.mark.django_db
def test_pipeline_test_task_default_queue():
    call_command("pipeline_test_task_default_queue")
