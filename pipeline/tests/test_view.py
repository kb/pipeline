# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from login.tests.factories import TEST_PASSWORD, UserFactory
from pipeline.models import (
    Pipeline,
    PipelineProcess,
    PipelineProcessStep,
    PipelineStep,
)
from .factories import PipelineProcessStepFactory


@pytest.mark.django_db
def test_process_delete(client):
    user = UserFactory(is_superuser=True)
    pipeline = Pipeline.objects.init_pipeline("apple", "Apple", False)
    PipelineStep.objects.init_pipeline_step(
        pipeline, 1, "Download from OneDrive", "msgraph", "service", "Download"
    )
    pipeline_process_step = PipelineProcess.objects.create_pipeline_process(
        pipeline
    )
    pipeline_process = pipeline_process_step.pipeline_process
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.post(
        reverse("pipeline.process.delete", args=[pipeline_process.pk])
    )
    # check
    assert HTTPStatus.FOUND == response.status_code
    assert response.url == reverse(
        "pipeline.process.detail",
        args=[pipeline_process.pk],
    )
    pipeline_process.refresh_from_db()
    assert pipeline_process.is_deleted is True
    assert user == pipeline_process.user_deleted


@pytest.mark.django_db
def test_process_step_retry(client):
    user = UserFactory(is_superuser=True)
    pipeline_process_step = PipelineProcessStepFactory(
        retries=PipelineProcessStep.DEFAULT_MAX_RETRY_COUNT
    )
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.post(
        reverse("pipeline.process.step.retry", args=[pipeline_process_step.pk])
    )
    # check
    assert HTTPStatus.FOUND == response.status_code
    assert response.url == reverse(
        "pipeline.process.detail",
        args=[pipeline_process_step.pipeline_process.pk],
    )
    pipeline_process_step.refresh_from_db()
    assert pipeline_process_step.max_retry_count == (
        pipeline_process_step.retries + 1
    )
