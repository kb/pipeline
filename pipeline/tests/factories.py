# -*- encoding: utf-8 -*-
import factory

from pipeline.models import (
    PipelineProcess,
    PipelineProcessStep,
    Pipeline,
    PipelineStep,
)


class PipelineFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Pipeline

    comment_required = False

    @factory.sequence
    def slug(n):
        return "slug_{}".format(n)

    @factory.sequence
    def title(n):
        return "title_{}".format(n)


class PipelineStepFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PipelineStep

    pipeline = factory.SubFactory(PipelineFactory)

    @factory.sequence
    def order(n):
        return n + 1

    @factory.sequence
    def class_name(n):
        return "report_class_{}".format(n + 1)


class PipelineProcessFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PipelineProcess

    pipeline = factory.SubFactory(PipelineFactory)


class PipelineProcessStepFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PipelineProcessStep

    max_retry_count = PipelineProcessStep.DEFAULT_MAX_RETRY_COUNT
    pipeline_process = factory.SubFactory(PipelineProcessFactory)
    pipeline_step = factory.SubFactory(PipelineStepFactory)
