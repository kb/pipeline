# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from login.tests.fixture import perm_check
from pipeline.models import (
    Pipeline,
    PipelineProcess,
    PipelineStep,
)
from .factories import PipelineProcessStepFactory


@pytest.mark.django_db
def test_process_delete(perm_check):
    pipeline = Pipeline.objects.init_pipeline("apple", "Apple", False)
    PipelineStep.objects.init_pipeline_step(
        pipeline, 1, "Download from OneDrive", "msgraph", "service", "Download"
    )
    pipeline_process_step = PipelineProcess.objects.create_pipeline_process(
        pipeline
    )
    url = reverse(
        "pipeline.process.delete",
        args=[pipeline_process_step.pipeline_process.pk],
    )
    perm_check.admin(url)


@pytest.mark.django_db
def test_process_detail(perm_check):
    pipeline = Pipeline.objects.init_pipeline("apple", "Apple", False)
    PipelineStep.objects.init_pipeline_step(
        pipeline, 1, "Download from OneDrive", "msgraph", "service", "Download"
    )
    pipeline_process_step = PipelineProcess.objects.create_pipeline_process(
        pipeline
    )
    url = reverse(
        "pipeline.process.detail",
        args=[pipeline_process_step.pipeline_process.pk],
    )
    perm_check.staff(url)


@pytest.mark.django_db
def test_process_list(perm_check):
    url = reverse("pipeline.process.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_process_outstanding_ignore_retries(perm_check):
    url = reverse("pipeline.process.outstanding.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_process_retry(perm_check):
    pipeline_process_step = PipelineProcessStepFactory()
    url = reverse(
        "pipeline.process.step.retry",
        args=[pipeline_process_step.pk],
    )
    perm_check.admin(url)


@pytest.mark.django_db
def test_process_step_list(perm_check):
    perm_check.staff(reverse("pipeline.process.step.list"))
