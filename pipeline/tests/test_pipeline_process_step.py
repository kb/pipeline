# -*- encoding: utf-8 -*-
import pytest

from django.utils import timezone

from login.tests.factories import UserFactory
from pipeline.models import PipelineProcessStep
from pipeline.tests.factories import (
    PipelineProcessFactory,
    PipelineProcessStepFactory,
)


@pytest.mark.django_db
def test_current():
    """Return the current steps.

    This is used by the ``outstanding`` method in ``RetryModelManager``.

    - Exclude steps where the ``pipeline_process`` has been deleted.
    - Completed items are excluded in the ``outstanding`` method.
    - The ``PipelineProcessStep`` (``RetryModel``) cannot be deleted.

    """
    # 1. completed (should be included)
    pipeline_process_1 = PipelineProcessFactory()
    pipeline_process_1_step = PipelineProcessStepFactory(
        pipeline_process=pipeline_process_1,
        completed_date=timezone.now(),
    )
    # 2.
    pipeline_process_2 = PipelineProcessFactory()
    pipeline_process_2_step = PipelineProcessStepFactory(
        pipeline_process=pipeline_process_2
    )
    # 3. retries exceed max (should be included)
    pipeline_process_3 = PipelineProcessFactory()
    pipeline_process_3_step = PipelineProcessStepFactory(
        pipeline_process=pipeline_process_3,
        max_retry_count=5,
        retries=7,
    )
    # 4. deleted process (exclude this one)!
    pipeline_process_4 = PipelineProcessFactory()
    pipeline_process_4.set_deleted(UserFactory())
    pipeline_process_4_step = PipelineProcessStepFactory(
        pipeline_process=pipeline_process_4
    )
    # 5.
    pipeline_process_5 = PipelineProcessFactory()
    pipeline_process_5_step = PipelineProcessStepFactory(
        pipeline_process=pipeline_process_5
    )
    # test
    assert set(
        [
            pipeline_process_1_step.pk,
            pipeline_process_2_step.pk,
            pipeline_process_3_step.pk,
            pipeline_process_5_step.pk,
        ]
    ) == set([x.pk for x in PipelineProcessStep.objects.current()])
