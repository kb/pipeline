# -*- encoding: utf-8 -*-
import pytest

from pipeline.models import (
    Pipeline,
    PipelineError,
    PipelineProcess,
    PipelineProcessStep,
    PipelineStep,
)


@pytest.mark.django_db
def test_run_does_not_exist():
    pipeline = Pipeline.objects.init_pipeline("apple", "Apple", False)
    PipelineStep.objects.init_pipeline_step(
        pipeline, 1, "Download from OneDrive", "msgraph", "service", "Download"
    )
    assert 0 == PipelineProcessStep.objects.count()
    PipelineProcess.objects.create_pipeline_process(pipeline)
    assert 1 == PipelineProcessStep.objects.count()
    pipeline_process_step = PipelineProcessStep.objects.first()
    with pytest.raises(PipelineError) as e:
        pipeline_process_step.process()
    assert (
        "Cannot find class 'Download'. Do you have 'service.py' in "
        "your 'msgraph' package? (No module named 'msgraph')"
    ) in str(e.value)


@pytest.mark.django_db
def test_str():
    pipeline = Pipeline.objects.init_pipeline("apple", "Apple", False)
    PipelineStep.objects.init_pipeline_step(
        pipeline, 1, "Download from OneDrive", "msgraph", "service", "Download"
    )
    assert 0 == PipelineProcessStep.objects.count()
    pipeline_process_step = PipelineProcess.objects.create_pipeline_process(
        pipeline
    )
    assert 1 == PipelineProcessStep.objects.count()
    pipeline_process_step = PipelineProcessStep.objects.first()
    assert "Pipeline process step {} for process {} (created ".format(
        pipeline_process_step.pk, pipeline_process_step.pipeline_process.pk
    ) in str(pipeline_process_step)
