# -*- encoding: utf-8 -*-
import pytest
import pytz

from datetime import datetime
from django.utils import timezone
from freezegun import freeze_time

from login.tests.factories import UserFactory
from pipeline.models import (
    Pipeline,
    PipelineError,
    PipelineProcess,
    PipelineStatus,
    PipelineStep,
)
from pipeline.tests.factories import (
    PipelineProcessFactory,
    PipelineProcessStepFactory,
)


@pytest.mark.django_db
def test_current():
    pipeline_process_1 = PipelineProcessFactory()
    pipeline_process_2 = PipelineProcessFactory()
    pipeline_process_2.set_deleted(UserFactory())
    pipeline_process_3 = PipelineProcessFactory()
    assert set([pipeline_process_1.pk, pipeline_process_3.pk]) == set(
        [x.pk for x in PipelineProcess.objects.current()]
    )


@pytest.mark.django_db
def test_init():
    pipeline = Pipeline.objects.init_pipeline("apple", "Apple", False)
    PipelineStep.objects.init_pipeline_step(
        pipeline, 1, "Download from OneDrive", "msgraph", "service", "Download"
    )
    PipelineStep.objects.init_pipeline_step(
        pipeline,
        2,
        "Update document properties",
        "docprops",
        "service",
        "Metadata",
    )
    pipeline_process_step = PipelineProcess.objects.create_pipeline_process(
        pipeline
    )
    assert pipeline == pipeline_process_step.pipeline_process.pipeline
    assert ["msgraph"] == [
        x.pipeline_step.app
        for x in pipeline_process_step.pipeline_process.steps()
    ]


@pytest.mark.django_db
def test_init_no_steps():
    pipeline = Pipeline.objects.init_pipeline("apple", "Apple", False)
    with pytest.raises(PipelineError) as e:
        PipelineProcess.objects.create_pipeline_process(pipeline)
    assert (
        "PipelineError, Cannot create 'PipelineProcess', "
        "'Pipeline' 'apple' has no steps"
    ) in str(e.value)


@pytest.mark.django_db
def test_order():
    pipeline_1 = Pipeline.objects.init_pipeline("apple", "Apple", False)
    PipelineStep.objects.init_pipeline_step(
        pipeline_1,
        1,
        "Download from OneDrive",
        "msgraph",
        "service",
        "Download",
    )
    PipelineProcess.objects.create_pipeline_process(pipeline_1)
    PipelineProcess.objects.create_pipeline_process(pipeline_1)
    pipeline_2 = Pipeline.objects.init_pipeline("orange", "Orange", False)
    PipelineStep.objects.init_pipeline_step(
        pipeline_2,
        1,
        "Update document properties",
        "docprops",
        "service",
        "Metadata",
    )
    PipelineProcess.objects.create_pipeline_process(pipeline_2)
    assert ["orange", "apple", "apple"] == [
        x.pipeline.slug for x in PipelineProcess.objects.all()
    ]


@pytest.mark.django_db
def test_outstanding_ignore_retries():
    pipeline_process_1 = PipelineProcessFactory()
    PipelineProcessStepFactory(
        pipeline_process=pipeline_process_1,
        completed_date=timezone.now(),
    )
    pipeline_process_2 = PipelineProcessFactory()
    PipelineProcessStepFactory(pipeline_process=pipeline_process_2)
    pipeline_process_3 = PipelineProcessFactory()
    PipelineProcessStepFactory(
        pipeline_process=pipeline_process_3,
        max_retry_count=5,
        retries=5,
    )
    pipeline_process_4 = PipelineProcessFactory()
    pipeline_process_4.set_deleted(UserFactory())
    PipelineProcessStepFactory(pipeline_process=pipeline_process_4)
    pipeline_process_5 = PipelineProcessFactory()
    PipelineProcessStepFactory(
        pipeline_process=pipeline_process_5,
        max_retry_count=5,
        retries=6,
    )
    assert set(
        [
            pipeline_process_2.pk,
            pipeline_process_3.pk,
            pipeline_process_5.pk,
        ]
    ) == set(
        [x.pk for x in PipelineProcess.objects.outstanding_ignore_retries()]
    )


@pytest.mark.django_db
def test_pipeline_process_step():
    pipeline = Pipeline.objects.init_pipeline("apple", "Apple", False)
    PipelineStep.objects.init_pipeline_step(
        pipeline, 1, "Download from OneDrive", "msgraph", "service", "Download"
    )
    with freeze_time(datetime(2020, 3, 11, 18, 30, 0, tzinfo=pytz.utc)):
        pipeline_process_step = PipelineProcess.objects.create_pipeline_process(
            pipeline
        )
    x = PipelineProcess.objects.step_for_pks(
        [pipeline_process_step.pipeline_process.pk]
    )
    assert pipeline_process_step.pipeline_process == x.pipeline_process


@pytest.mark.django_db
def test_step_for_pks():
    pipeline_process = PipelineProcessFactory()
    with freeze_time(datetime(2020, 1, 1, 9, 5, 0, tzinfo=pytz.utc)):
        PipelineProcessStepFactory(pipeline_process=pipeline_process)
    with freeze_time(datetime(2020, 1, 1, 9, 4, 0, tzinfo=pytz.utc)):
        PipelineProcessStepFactory(pipeline_process=pipeline_process)
    with freeze_time(datetime(2020, 1, 1, 9, 3, 0, tzinfo=pytz.utc)):
        process_step_3 = PipelineProcessStepFactory(
            pipeline_process=pipeline_process
        )
    x = PipelineProcess.objects.step_for_pks([pipeline_process.pk])
    assert x == process_step_3


@pytest.mark.django_db
def test_pipeline_status():
    pipeline = Pipeline.objects.init_pipeline("apple", "Apple", False)
    PipelineStep.objects.init_pipeline_step(
        pipeline, 1, "Download from OneDrive", "msgraph", "service", "Download"
    )
    with freeze_time(datetime(2020, 3, 11, 18, 30, 0, tzinfo=pytz.utc)):
        pipeline_process_step = PipelineProcess.objects.create_pipeline_process(
            pipeline
        )
    assert PipelineStatus(
        pending=True,
        onedrive_folder_name="wip",
        status="Download from OneDrive (pending since 11/03/2020 18:30)",
    ) == PipelineProcess.objects.pipeline_status(
        [pipeline_process_step.pipeline_process.pk], "wip"
    )


@pytest.mark.django_db
def test_str():
    pipeline = Pipeline.objects.init_pipeline("apple", "Apple", False)
    PipelineStep.objects.init_pipeline_step(
        pipeline, 1, "Download from OneDrive", "msgraph", "service", "Download"
    )
    pipeline_process_step = PipelineProcess.objects.create_pipeline_process(
        pipeline
    )
    assert "Process {} for pipeline 'apple' (created".format(
        pipeline_process_step.pipeline_process.pk
    ) in str(pipeline_process_step.pipeline_process)
