# -*- encoding: utf-8 -*-
import pytest

from login.tests.factories import UserFactory
from pipeline.models import Pipeline, PipelineStep
from .factories import PipelineFactory, PipelineStepFactory


@pytest.mark.django_db
def test_init():
    pipeline = PipelineFactory()
    x = PipelineStep.objects.init_pipeline_step(
        pipeline, 7, "Download from OneDrive", "msgraph", "service", "Download"
    )
    assert pipeline == x.pipeline
    assert 7 == x.order
    assert "msgraph" == x.app
    assert "service" == x.module
    assert "Download" == x.class_name


@pytest.mark.django_db
def test_init_already_exists():
    pipeline = PipelineFactory()
    PipelineStepFactory(pipeline=pipeline, order=7)
    assert 1 == PipelineStep.objects.count()
    x = PipelineStep.objects.init_pipeline_step(
        pipeline,
        7,
        "Download from OneDrive",
        "msgraph",
        "service",
        "Download",
    )
    assert 1 == PipelineStep.objects.count()
    assert pipeline == x.pipeline
    assert 7 == x.order
    assert "msgraph" == x.app
    assert "service" == x.module
    assert "Download" == x.class_name
    assert "Download from OneDrive" == x.description


@pytest.mark.django_db
def test_init_deleted():
    pipeline = PipelineFactory()
    x = PipelineStepFactory(pipeline=pipeline, order=7)
    x.set_deleted(UserFactory())
    x.refresh_from_db()
    assert x.is_deleted is True
    assert 1 == PipelineStep.objects.count()
    x = PipelineStep.objects.init_pipeline_step(
        pipeline,
        7,
        "Download from OneDrive",
        "msgraph",
        "service",
        "Download",
    )
    x.refresh_from_db()
    assert x.is_deleted is False


@pytest.mark.django_db
def test_order():
    PipelineStepFactory(pipeline=PipelineFactory(slug="orange"), order=3)
    PipelineStepFactory(pipeline=PipelineFactory(slug="pear"), order=5)
    PipelineStepFactory(pipeline=PipelineFactory(slug="apple"), order=1)
    assert [1, 3, 5] == [x.order for x in PipelineStep.objects.all()]


@pytest.mark.django_db
def test_str():
    x = PipelineStepFactory(
        pipeline=PipelineFactory(slug="apple"),
        order=3,
        class_name="DownloadDocument",
    )
    assert "'apple' process - step 3: 'DownloadDocument'" == str(x)


@pytest.mark.django_db
def test_usage():
    pipeline = Pipeline.objects.init_pipeline("apple", "Apple", False)
    PipelineStep.objects.init_pipeline_step(
        pipeline, 1, "Download from OneDrive", "msgraph", "service", "Download"
    )
    PipelineStep.objects.init_pipeline_step(
        pipeline,
        2,
        "Update document metadata",
        "docprops",
        "service",
        "Metadata",
    )
    PipelineStep.objects.init_pipeline_step(
        pipeline, 3, "Upload to OneDrive", "msgraph", "service", "Upload"
    )
    assert 3 == PipelineStep.objects.count()
    assert [1, 2, 3] == [x.order for x in PipelineStep.objects.all()]
    assert ["Download", "Metadata", "Upload"] == [
        x.class_name for x in PipelineStep.objects.all()
    ]
