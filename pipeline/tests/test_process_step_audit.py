# -*- encoding: utf-8 -*-
import pytest

from django.utils import timezone

from pipeline.models import (
    Pipeline,
    PipelineProcess,
    PipelineProcessStep,
    PipelineProcessStepAudit,
    PipelineStep,
)


def _pipeline_process_step(class_name):
    pipeline = Pipeline.objects.init_pipeline("apple", "Apple", False)
    PipelineStep.objects.init_pipeline_step(
        pipeline, 1, class_name, "example_pipeline", "service", class_name
    )
    PipelineStep.objects.init_pipeline_step(
        pipeline, 2, "Document upload", "example_pipeline", "service", "Upload"
    )
    assert 0 == PipelineProcessStep.objects.count()
    PipelineProcess.objects.create_pipeline_process(pipeline)
    assert 1 == PipelineProcessStep.objects.count()
    return PipelineProcessStep.objects.first()


@pytest.mark.django_db
def test_ordering():
    pipeline_process_step = _pipeline_process_step("Download")
    PipelineProcessStepAudit.objects.create_pipeline_process_step_audit(
        pipeline_process_step, "A"
    )
    PipelineProcessStepAudit.objects.create_pipeline_process_step_audit(
        pipeline_process_step, "B"
    )
    assert ["B", "A"] == [
        x.description for x in PipelineProcessStepAudit.objects.all()
    ]


@pytest.mark.django_db
def test_str():
    pipeline_process_step = _pipeline_process_step("Download")
    x = PipelineProcessStepAudit.objects.create_pipeline_process_step_audit(
        pipeline_process_step, "Oranges and Lemons"
    )
    assert "Oranges and Lemons" in str(x)
    assert str(pipeline_process_step.pk) in str(x)
    assert timezone.now().date().strftime("%d/%m/%Y") in str(x)
