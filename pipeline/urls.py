# -*- encoding: utf-8 -*-
from django.urls import re_path

from .views import (
    PipelineProcessDeleteView,
    PipelineProcessDetailView,
    PipelineProcessListView,
    PipelineProcessOutstandingIgnoreRetriesListView,
    PipelineProcessStepListView,
    PipelineProcessStepRetryUpdateView,
)


urlpatterns = [
    re_path(
        r"^process/$",
        view=PipelineProcessListView.as_view(),
        name="pipeline.process.list",
    ),
    re_path(
        r"^process/(?P<pk>\d+)/delete/$",
        view=PipelineProcessDeleteView.as_view(),
        name="pipeline.process.delete",
    ),
    re_path(
        r"^process/(?P<pk>\d+)/$",
        view=PipelineProcessDetailView.as_view(),
        name="pipeline.process.detail",
    ),
    re_path(
        r"^process/outstanding/$",
        view=PipelineProcessOutstandingIgnoreRetriesListView.as_view(),
        name="pipeline.process.outstanding.list",
    ),
    re_path(
        r"^process/step/$",
        view=PipelineProcessStepListView.as_view(),
        name="pipeline.process.step.list",
    ),
    re_path(
        r"^process/step/(?P<pk>\d+)/retry/$",
        view=PipelineProcessStepRetryUpdateView.as_view(),
        name="pipeline.process.step.retry",
    ),
]
