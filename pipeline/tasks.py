# -*- encoding: utf-8 -*-
import dramatiq
import logging

from django.conf import settings


logger = logging.getLogger(__name__)


def pipeline_process_step(task_name, process_step_pk):
    """Process a pipeline process step."""
    logger.info("{} {}".format(task_name, process_step_pk))
    from pipeline.models import PipelineError, PipelineProcessStep

    # If the pipeline step has already been complete (perhaps via a retry)
    # https://www.kbsoftware.co.uk/crm/ticket/5423/
    pipeline_process_step = PipelineProcessStep.objects.get(pk=process_step_pk)
    if pipeline_process_step.is_complete():
        pass
    else:
        # the 'process' method is found in ``RetryModelManager`` (``base`` app)
        result = PipelineProcessStep.objects.process(process_step_pk)
        if result:
            logger.info(
                "{} {} (success): {}".format(task_name, process_step_pk, result)
            )
        else:
            message = "{} {} (fail): {}".format(
                task_name, process_step_pk, result
            )
            logger.error(message)
            raise PipelineError(message)


# https://dramatiq.io/cookbook.html#binding-worker-groups-to-queues
@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME_PIPELINE)
def process_next_step(process_step_pk):
    logger.info("pipeline.tasks.process_outstanding_steps")
    from pipeline.models import PipelineProcessStep

    process_step = None
    try:
        process_step = PipelineProcessStep.objects.get(pk=process_step_pk)
    except PipelineProcessStep.DoesNotExist:
        logger.error(
            "pipeline.tasks.process_next_step: "
            "PipelineProcessStep {} does not exist".format(process_step_pk)
        )
    if process_step:
        if process_step.is_complete():
            logger.info(
                "pipeline.tasks.process_next_step: "
                "PipelineProcessStep {} is already complete".format(
                    process_step.pk
                )
            )
        else:
            pipeline_process_step(
                "pipeline.tasks.process_next_step", process_step.pk
            )


# https://dramatiq.io/cookbook.html#binding-worker-groups-to-queues
@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME_PIPELINE, max_retries=0)
def process_outstanding_steps():
    """Process *all* outstanding steps.

    To process a single step (recommended) use the ``process_step`` function
    (see below).

    .. warning:: Only call this function as a clean-up e.g. in the middle of the
                 night or when running a one-off process e.g. data transfer.
                 If this function is called several times in quick succession,
                 it may try and process steps multiple times.

    """
    logger.info("pipeline.tasks.process_outstanding_steps")
    from pipeline.models import PipelineProcessStep

    # the 'process' method is found in ``RetryModelManager`` (``base`` app)
    PipelineProcessStep.objects.process()
    logger.info("pipeline.tasks.process_outstanding_steps - complete")


# https://dramatiq.io/cookbook.html#binding-worker-groups-to-queues
@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME_PIPELINE)
def process_step(process_step_pk):
    """Process an outstanding step."""
    pipeline_process_step("pipeline.tasks.process_step", process_step_pk)


# https://dramatiq.io/cookbook.html#binding-worker-groups-to-queues
@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME_PIPELINE)
def pipeline_test_task():
    logger.info("pipeline.tasks.pipeline_test_task")


# https://dramatiq.io/cookbook.html#binding-worker-groups-to-queues
@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def pipeline_test_task_default_queue():
    logger.info("pipeline.tasks.pipeline_test_task_default_queue")
