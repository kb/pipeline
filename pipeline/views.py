# -*- encoding: utf-8 -*-
from braces.views import (
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    SuperuserRequiredMixin,
)
from django.contrib import messages
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.db import transaction
from django.urls import reverse
from django.views.generic import DetailView, ListView, UpdateView

from base.view_utils import BaseMixin, RedirectNextMixin
from pipeline.forms import (
    PipelineProcessEmptyForm,
    PipelineProcessStepEmptyForm,
)
from pipeline.models import PipelineProcess, PipelineProcessStep
from pipeline.tasks import process_step


class PipelineProcessDeleteView(
    LoginRequiredMixin,
    SuperuserRequiredMixin,
    RedirectNextMixin,
    BaseMixin,
    UpdateView,
):
    form_class = PipelineProcessEmptyForm
    model = PipelineProcess

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.set_deleted(self.request.user)
            messages.info(
                self.request,
                "Deleted process '{}' ({})".format(
                    self.object.pk,
                    self.object.pipeline.title,
                ),
            )
            result = super().form_valid(form)
        return result

    def get_success_url(self):
        next_url = self.request.POST.get(REDIRECT_FIELD_NAME)
        if next_url:
            return next_url
        else:
            return reverse("pipeline.process.detail", args=[self.object.pk])


class PipelineProcessDetailView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, DetailView
):
    model = PipelineProcess


class PipelineProcessListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    """List *all* pipeline processes (including deleted)."""

    model = PipelineProcess
    paginate_by = 20


class PipelineProcessOutstandingIgnoreRetriesListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(title="Pipeline Process - Outstanding"))
        return context

    def get_queryset(self):
        return PipelineProcess.objects.outstanding_ignore_retries()


class PipelineProcessStepRetryUpdateView(
    LoginRequiredMixin,
    SuperuserRequiredMixin,
    RedirectNextMixin,
    BaseMixin,
    UpdateView,
):
    form_class = PipelineProcessStepEmptyForm
    model = PipelineProcessStep

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.max_retry_count = self.object.retries + 1
            messages.info(
                self.request,
                "Retry process step '{}' ({})".format(
                    self.object.pk,
                    self.object.pipeline_step.description,
                ),
            )
            transaction.on_commit(
                lambda: process_step.send_with_options(args=(self.object.pk,))
            )
            result = super().form_valid(form)
        return result

    def get_success_url(self):
        next_url = self.request.POST.get(REDIRECT_FIELD_NAME)
        if next_url:
            return next_url
        else:
            return reverse(
                "pipeline.process.detail",
                args=[self.object.pipeline_process.pk],
            )


class PipelineProcessStepListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    model = PipelineProcessStep
    paginate_by = 20
