# Generated by Django 3.0.6 on 2020-08-18 14:52

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("pipeline", "0002_pipeline_comment_required"),
    ]

    operations = [
        migrations.AddField(
            model_name="pipelineprocess",
            name="date_deleted",
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="pipelineprocess",
            name="deleted",
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name="pipelineprocess",
            name="user_deleted",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="+",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
        migrations.AddField(
            model_name="pipelineprocessstep",
            name="max_retry_count",
            field=models.PositiveIntegerField(default=5),
            preserve_default=False,
        ),
    ]
