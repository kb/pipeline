# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from pipeline.tasks import pipeline_test_task


class Command(BaseCommand):
    help = "Pipeline - test task (pipeline queue)"

    def handle(self, *args, **options):
        self.stdout.write("{}...".format(self.help))
        pipeline_test_task.send()
        self.stdout.write("{} - Complete".format(self.help))
