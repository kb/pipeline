# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from pipeline.tasks import process_outstanding_steps


class Command(BaseCommand):
    help = "Pipeline - process outstanding steps"

    def handle(self, *args, **options):
        self.stdout.write("{}...".format(self.help))
        process_outstanding_steps.send()
        self.stdout.write("{} - Complete".format(self.help))
