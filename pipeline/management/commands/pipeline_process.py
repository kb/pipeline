# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from pipeline.models import PipelineProcessStep


class Command(BaseCommand):
    help = "Pipeline - process"

    def handle(self, *args, **options):
        self.stdout.write("{}...".format(self.help))
        PipelineProcessStep.objects.process()
        self.stdout.write("{} - Complete".format(self.help))
