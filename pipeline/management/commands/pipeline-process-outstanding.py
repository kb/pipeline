# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from pipeline.models import PipelineProcess


class Command(BaseCommand):
    help = "List outstanding pipeline processes #6700"

    def handle(self, *args, **options):
        self.stdout.write(self.help)
        qs = PipelineProcess.objects.outstanding_ignore_retries()
        self.stdout.write(
            f"Found {qs.count()} outstanding pipeline processes..."
        )
        for counter, pipeline_process in enumerate(qs, start=1):
            self.stdout.write(
                f"{counter:>4}. {pipeline_process.pk}  "
                f"{pipeline_process.created.strftime('%d/%m/%Y %H:%M')}  "
                f"{pipeline_process.pipeline.title:<40}"
                f"{pipeline_process.pipeline.slug:<20}  "
            )
        self.stdout.write(f"{self.help} - Complete...")
