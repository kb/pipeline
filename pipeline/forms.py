# -*- encoding: utf-8 -*-
from django import forms
from .models import PipelineProcess, PipelineProcessStep


class PipelineProcessEmptyForm(forms.ModelForm):
    class Meta:
        model = PipelineProcess
        fields = ()


class PipelineProcessStepEmptyForm(forms.ModelForm):
    class Meta:
        model = PipelineProcessStep
        fields = ()
