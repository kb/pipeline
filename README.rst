Steps
*****

List of tasks which all need to be done...

Current Models::

  Sequence
  Step

  Process
  ProcessStep
  ProcessStepAudit

Revised::

  Sequence
  Step

  SequenceInstance
  StepInstance
  StepInstanceAudit

Revised Again::

  Pipeline
  Step

  Process
  Task
  TaskAudit

https://chat.kbsoftware.co.uk/kb/pl/xh37eagh93nxxyzg7oagf5nrur

Install
=======

Virtual Environment
-------------------

::

  virtualenv --python=python3 venv-pipeline
  # or
  python3 -m venv venv-pipeline

  source venv-pipeline/bin/activate

  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  pytest -x

Usage
=====

::

  ./init_dev.sh

For configuration and testing, check the documentation at:
https://www.kbsoftware.co.uk/docs/app-pipeline.html

Release
=======

https://www.kbsoftware.co.uk/docs/
